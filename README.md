# Blog Wise

A blogging application. Formatted as a demonstration of the following:

* MVP branch
* Sprint 1 branch: Planning documents, project skeleton (working server)
* Sprint 2 branch: MVP
* Sprint 3 branch: Final
* Master branch: Full featured app

# Sprint 1

All data is user generated. No external APIs. I will use fake data for the MVP.

Stories: 

- User can register and login
- User can update their profile
- User can create articles
- User can read any articles
- User can update their own articles

## Models

__Overview__
* User
* Article

## Wireframes

__Overview__


## TODO

- Data modeling
- Seed file
- Base template with bootstrap
- Running server
